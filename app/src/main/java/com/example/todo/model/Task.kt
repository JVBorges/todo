package com.example.todo.model

class Task constructor(
  private var description: String,
  private var isUrgent: Boolean,
  private var isDone: Boolean,
){
  var id: Long = 0

  fun getDescription(): String = this.description
  fun isUrgent(): Boolean = this.isUrgent
  fun isDone(): Boolean = this.isDone

  fun setIsDone(isDone: Boolean) {
    this.isDone = isDone
  }

  fun setUrgency(isUrgent: Boolean) {
    this.isUrgent = isUrgent
  }

  fun setDescription(description: String) {
    this.description = description
  }

  override fun toString(): String = "${if (isUrgent) "(Urgent):" else "(Not Urgent):"} $description"
}