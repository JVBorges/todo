package com.example.todo.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(context: Context): SQLiteOpenHelper(context, DBNAME, null, DBVERSION) {
  companion object {
    private const val DBNAME = "pdm_todo_app.db"
    private const val DBVERSION = 1
  }

  override fun onCreate(db: SQLiteDatabase?) {
    db?.execSQL(DBSchema.TaskTable.getCreateTableQuery())
  }

  override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
    TODO("Not yet implemented")
  }

}