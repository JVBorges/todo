package com.example.todo.data

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import com.example.todo.model.Task

class DBTaskTableManager(context: Context) {
  companion object {
    const val WHEREID = "${DBSchema.TaskTable.ID} = ?"
    val COLS = arrayOf(DBSchema.TaskTable.ID, DBSchema.TaskTable.DESCRIPTION, DBSchema.TaskTable.URGENT, DBSchema.TaskTable.DONE)
    val ORDERBY = "${DBSchema.TaskTable.TIMESTAMP}"
  }

  private val dbHelper = DBHelper(context)

  fun insert(task: Task): Long {
    val cv = ContentValues()
    cv.put(DBSchema.TaskTable.DESCRIPTION, task.getDescription())
    cv.put(DBSchema.TaskTable.URGENT, task.isUrgent())
    cv.put(DBSchema.TaskTable.DONE, task.isDone())
    val db = this.dbHelper.writableDatabase
    val id = db.insert(DBSchema.TaskTable.TABLENAME, null, cv)
    db.close()
    return id
  }

  fun delete(task: Task) {
    val db = this.dbHelper.writableDatabase
    db.delete(DBSchema.TaskTable.TABLENAME, WHEREID, arrayOf(task.id.toString()))
    db.close()
  }

  fun update(task: Task) {
    val cv = ContentValues()
    cv.put(DBSchema.TaskTable.DESCRIPTION, task.getDescription())
    cv.put(DBSchema.TaskTable.URGENT, task.isUrgent())
    cv.put(DBSchema.TaskTable.DONE, task.isDone())
    val db = this.dbHelper.writableDatabase
    db.update(DBSchema.TaskTable.TABLENAME, cv, WHEREID, arrayOf(task.id.toString()))
    db.close()
  }

  @SuppressLint("Range")
  fun getAllTasks(): ArrayList<Task> {
    val tasks: ArrayList<Task> = ArrayList()
    val db = this.dbHelper.writableDatabase
    val cur = db.query(DBSchema.TaskTable.TABLENAME, COLS, null, null, null, null, ORDERBY)

    while (cur.moveToNext()) {
      val id = cur.getLong(cur.getColumnIndex(DBSchema.TaskTable.ID))
      val description = cur.getString(cur.getColumnIndex(DBSchema.TaskTable.DESCRIPTION))
      val isUrgent = cur.getInt(cur.getColumnIndex(DBSchema.TaskTable.URGENT))
      val isDone = cur.getInt(cur.getColumnIndex(DBSchema.TaskTable.DONE))
      val task = Task(description, isUrgent == 1, isDone == 1)
      task.id = id
      tasks.add(task)
    }
    db.close()
    return tasks
  }
}