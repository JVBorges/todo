package com.example.todo.data

import android.content.Context
import com.example.todo.model.Task

object TaskDAOSingleton {
  private lateinit var tasks: ArrayList<Task>
  private lateinit var dbTask: DBTaskTableManager

  private fun initDAO(context: Context) {
    if (!::tasks.isInitialized) {
      this.dbTask = DBTaskTableManager(context)
      this.tasks = this.dbTask.getAllTasks()
    }
  }

  fun add(context: Context, task: Task): Int {
    this.initDAO(context)
    task.id = this.dbTask.insert(task)
    tasks.add(task)
    return 0
  }

  fun update(context: Context, task: Task): Int {
    this.initDAO(context)
    val pos = this.tasks.indexOf(task)
    val tk = this.tasks[pos]

    if (task.getDescription().isNotEmpty())
      tk.setDescription(task.getDescription())

    tk.setUrgency(task.isUrgent())
    tk.setIsDone(task.isDone())

    this.dbTask.update(tk)
    return pos
  }

  fun delete(context: Context, task: Task): Int {
    this.initDAO(context)
    val pos = this.tasks.indexOf(task)
    this.tasks.removeAt(pos)
    this.dbTask.delete(task)
    return pos
  }

  fun getTasks(context: Context): ArrayList<Task> {
    this.initDAO(context)
    return this.tasks
  }
}