package com.example.todo.ui.dialogs

import android.app.AlertDialog
import android.content.Context
import com.example.todo.R

abstract class ConfirmTaskDeleteOperationDialog(private val context: Context) {
  init {
    AlertDialog.Builder(this.context)
      .setTitle(R.string.delete_task_operation)
      .setMessage(R.string.r_u_sure_del_task)
      .setPositiveButton(R.string.yes) { _, _ ->
        this.onConfirm()
      }
      .setNegativeButton(R.string.no, null)
      .create().show()
  }
  abstract fun onConfirm()

}