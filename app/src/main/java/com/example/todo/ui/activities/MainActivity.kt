package com.example.todo.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import androidx.core.util.forEach
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.R
import com.example.todo.data.TaskDAOSingleton
import com.example.todo.model.Task
import com.example.todo.ui.dialogs.ConfirmTaskDeleteOperationDialog
import com.example.todo.ui.dialogs.EditTaskDialog
import com.example.todo.ui.lists.TaskListAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
  private lateinit var txtInput: EditText
  private lateinit var switchUrgent: SwitchCompat
  private lateinit var cbHideDone: CheckBox

  private lateinit var taskDAO: TaskDAOSingleton

  private lateinit var taskListData: ArrayList<Task>
  private lateinit var taskRecyclerView: RecyclerView
  private lateinit var taskListAdapter: TaskListAdapter

  private lateinit var fabDeleteTask: FloatingActionButton
  private lateinit var fabEditTask: FloatingActionButton


  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    this.txtInput = findViewById(R.id.txtInput)
    this.switchUrgent = findViewById(R.id.urgentSwitch)
    this.cbHideDone = findViewById(R.id.cbHideDone)
    this.taskRecyclerView = findViewById(R.id.recyclerView)
    this.fabDeleteTask = findViewById(R.id.fabDeleteTask)
    this.fabEditTask = findViewById(R.id.fabEditTask)
    this.taskDAO = TaskDAOSingleton

    this.taskListData = this.taskDAO.getTasks(this)
    this.taskListAdapter = TaskListAdapter(this.taskListData)
      .setOnChangeSelectionListener {
        if(it.size() > 0) {
          this.fabDeleteTask.visibility = View.VISIBLE
          this.fabEditTask.visibility = if(it.size() > 1) { View.GONE } else { View.VISIBLE }
        }
        else {
          this.fabDeleteTask.visibility = View.GONE
          this.fabEditTask.visibility = View.GONE
        }
      }

    this.taskRecyclerView.layoutManager = LinearLayoutManager(this)
    this.taskRecyclerView.layoutManager = LinearLayoutManager(this)
    this.taskRecyclerView.setHasFixedSize(true)
    this.taskRecyclerView.adapter = this.taskListAdapter
  }

  fun onClickCreateTask(view: View) {
    val inputStr: String = this.txtInput?.text.toString()

    if (inputStr.isNotEmpty()) {
      var isUrgent: Boolean = this.switchUrgent?.isChecked
      var task: Task = Task(inputStr, isUrgent, false)

      this.taskDAO.add(baseContext, task)
      this.taskRecyclerView.adapter?.notifyItemInserted(this.taskListData.size - 1)
    } else {
      Toast.makeText(this, R.string.empty_input, Toast.LENGTH_SHORT).show()
    }
  }

  fun onClickHideDone(view: View) {
    val isChecked = this.cbHideDone.isChecked
    (this.taskRecyclerView.adapter as TaskListAdapter).filterDone(!isChecked)
  }

  fun onClickDeleteSelectedTask(v: View) {
    object : ConfirmTaskDeleteOperationDialog(this) {
      override fun onConfirm() {
        taskListAdapter.selectedTasks.forEach { _, value ->
          val pos = taskDAO.delete(baseContext, value)
          taskListAdapter.notifyItemRemoved(pos)
        }
        taskListAdapter.selectedTasks.clear()
        fabDeleteTask.visibility = View.GONE
        fabEditTask.visibility = View.GONE
      }
    }
  }

  fun onClickUpdateSelectedTask(v: View) {
    object : EditTaskDialog(this) {
      override fun onClickOk(description: String, isUrgent: Boolean) {
        try {
          val task = taskListAdapter.selectedTasks.valueAt(0)
          if (description.isNotEmpty()) {
            task.setDescription(description)
          }
          task.setUrgency(isUrgent)
          val pos = taskDAO.update(baseContext, task)
          taskListAdapter.notifyItemChanged(pos)
        } catch (e: ArrayIndexOutOfBoundsException) {
          Log.wtf("ERROR_CCLACTIVITY", e.message)
          e.printStackTrace()
        }
      }
    }
  }

}