package com.example.todo.ui.lists

import android.annotation.SuppressLint
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.R
import com.example.todo.model.Task

class TaskListAdapter(private var taskList: ArrayList<Task>) :
  RecyclerView.Adapter<TaskListViewHolder>() {

  private val taskListBackup = this.taskList
  private var listener: OnClickTaskListener? = null
  private var selectionListener: OnChangeSelectionListener? = null

  val selectedTasks: SparseArray<Task> = SparseArray()
  var lastTask: Task? = null


  fun interface OnClickTaskListener {
    fun onRequestTask(task: Task)
  }

  fun interface OnChangeSelectionListener {
    fun onChangeSelection(selectedIds: SparseArray<Task>)
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskListViewHolder {
    val inflater = LayoutInflater.from(parent.context)
    val itemView = inflater.inflate(R.layout.view_task, parent, false)
    return TaskListViewHolder(itemView, this)
  }

  override fun onBindViewHolder(holder: TaskListViewHolder, position: Int) {
    holder.bind(this.taskList[position])
  }

  override fun getItemCount(): Int {
    return taskList.size
  }

  @SuppressLint("NotifyDataSetChanged")
  fun filterDone(turnOff: Boolean) {
    this.taskList = if (!turnOff) this.taskList.filter { task -> !task.isDone() } as ArrayList<Task> else taskListBackup
    notifyDataSetChanged()
  }

  fun setOnClickTaskListener(listener: OnClickTaskListener?): TaskListAdapter {
    this.listener = listener
    return this
  }
  fun getOnClickTaskListener(): OnClickTaskListener? {
    return this.listener
  }

  fun setOnChangeSelectionListener(selectionListener: OnChangeSelectionListener?): TaskListAdapter {
    this.selectionListener = selectionListener
    return this
  }

  fun getOnChangeSelectionListener(): OnChangeSelectionListener? {
    return this.selectionListener
  }

  fun updateLastRequestedTask() {
    if(this.lastTask != null)
      this.notifyItemChanged(this.taskList.indexOf(this.lastTask))
  }

}