package com.example.todo.ui.lists

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.R
import com.example.todo.data.TaskDAOSingleton
import com.example.todo.model.Task

class TaskListViewHolder(itemView: View, private val adapter: TaskListAdapter) : RecyclerView.ViewHolder(itemView), View.OnClickListener, View.OnLongClickListener {
  private val taskDAO: TaskDAOSingleton = TaskDAOSingleton
  private val txtTask: TextView = itemView.findViewById(R.id.txtTask)
  private val cbDone: CheckBox = itemView.findViewById(R.id.cbDone)
  private lateinit var task: Task
  private lateinit var background: Drawable

  init {
    itemView.setOnClickListener(this)
    itemView.setOnLongClickListener(this)
  }

  fun bind(task: Task) {
    this.background = ContextCompat.getDrawable(itemView.context, if (task.isUrgent()) R.drawable.red_left_border else R.drawable.green_left_border)!!
    this.task = task
    this.txtTask.text = this.task.getDescription()
    this.txtTask.background = this.background
    this.cbDone.isChecked = this.task.isDone()

    itemView.isSelected = this.adapter.selectedTasks.get(this.task.id.toInt()) != null

    cbDone.setOnClickListener {
      task.setIsDone(cbDone.isChecked)
      this.taskDAO.update(itemView.context, task)
    }
  }

  override fun onClick(v: View?) {
    this.adapter.getOnClickTaskListener()?.onRequestTask(this.task)
    this.adapter.lastTask = this.task
  }

  override fun onLongClick(v: View?): Boolean {
    itemView.isSelected = !itemView.isSelected
    if(this.adapter.selectedTasks.get(this.task.id.toInt()) != null)
      this.adapter.selectedTasks.delete(this.task.id.toInt())
    else
      this.adapter.selectedTasks.append(this.task.id.toInt(), this.task)

    val newSelectedArray = this.adapter.selectedTasks.clone()
    this.adapter.getOnChangeSelectionListener()?.onChangeSelection(newSelectedArray)
    return true
  }

}