package com.example.todo.ui.dialogs

import android.app.AlertDialog
import android.content.Context
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.widget.SwitchCompat
import com.example.todo.R

abstract class EditTaskDialog(private val context: Context) {
  private val layout = LinearLayout(this.context)
  private val etxtNewDescription = EditText(this.context)
  private val switchUrgente = SwitchCompat(this.context)

  init {
    layout.orientation = LinearLayout.VERTICAL
    layout.layoutParams = LinearLayout.LayoutParams(
      LinearLayout.LayoutParams.MATCH_PARENT,
      LinearLayout.LayoutParams.MATCH_PARENT
    )
    layout.addView(etxtNewDescription)
    layout.addView(switchUrgente)

    switchUrgente.setText(R.string.switchText)
    etxtNewDescription.setHint(R.string.type_new_task)

    AlertDialog.Builder(this.context)
      .setTitle(R.string.task_settings)
      .setView(layout)
      .setPositiveButton(R.string.ok) { _, _ ->
        val channelName = this.etxtNewDescription.text.toString()
        val isUrgent = this.switchUrgente.isChecked
        this.onClickOk(channelName, isUrgent)
      }
      .create().show()
  }
  abstract fun onClickOk(channelName: String, isUrgent: Boolean)

}